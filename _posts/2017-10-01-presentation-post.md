---
layout: post
title: Repositorio de Mineria de datos
cover: coverU.jpg
date:   2017-10-01 12:00:00
categories: posts
---

## Biblioteca Mineria de Datos.

Repositorio de proyectos de mineria de datos.

## Otras Comunidades en los Repositorios:

- [Arquitectura del Software](http://arsoft12.gitlab.io/)
  - [Documentos](http://arsoft12.gitlab.io/posts/)
  - [Presentaciones](http://arsoft12.gitlab.io/slide/)
- [Arquitectura del Hardware](http://arquh11.gitlab.io/)
  - [Documentos](http://arquh11.gitlab.io/posts/)
  - [Presentaciones](http://arquh11.gitlab.io/slide/)
